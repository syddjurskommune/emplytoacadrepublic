﻿using System;
using System.IO;
using System.Linq;

namespace EmplySFTPToAcadre
{
	internal class AcadreService
	{
		public AcadreService()
		{
		}

		internal int CreateCase(EmployeeInformation employeeInformation)
		{
			string caseID = "0";
			try
			{
				if (!CPRBrokerWrapper.IsValidCPR(employeeInformation.cpr)) throw new Exception("Det indtastede CPR-nummer ("+employeeInformation.cpr+") er ikke i formatet ddmmyy-xxxx." + Properties.Settings.Default.Fejl40indikator);

				string username = Properties.Settings.Default.AcadreAPI_Username;
				string password = Properties.Settings.Default.AcadreAPI_Password;
				string domain = Properties.Settings.Default.AcadreAPI_domain;
                string KLE = "81.03.00";


                var caseService = new AcadreServiceV7.CaseService7
				{
					Credentials = new System.Net.NetworkCredential(username, password, domain)
				};
				var contactService = new AcadreServiceV7.ContactService7
				{
					Credentials = new System.Net.NetworkCredential(username, password, domain)
				};
				var configurationService = new AcadreServiceV7.ConfigurationService7
				{
					Credentials = new System.Net.NetworkCredential(username, password, domain)
				};
				var documentService = new AcadreServiceV7.MainDocumentService7
				{
					Credentials = new System.Net.NetworkCredential(username, password, domain)
				};

				// Undersøger om sagen allerede eksisterer
				bool CaseExists = false;
				AcadreServiceV7.AdvancedSearchCaseCriterionType2 searchCriterion = new AcadreServiceV7.AdvancedSearchCaseCriterionType2();
				searchCriterion.CaseFileTitleText = employeeInformation.cpr;
				searchCriterion.CaseFileTypeCode = "PERSAG";
				searchCriterion.ClassificationCriterion = new AcadreServiceV7.ClassificationCriterionType[]
				{
						new AcadreServiceV7.ClassificationCriterionType()
						{
							ClassificationLiteral = KLE,
							PrincipleLiteral = "KL Koder"
						}
				};
				AcadreServiceV7.AdministrativeUnitType AdmUnit = new AcadreServiceV7.AdministrativeUnitType();
				AdmUnit.AdministrativeUnitReference = employeeInformation.AcadreOrgId.ToString(); // AcadreOrgID fra view, 268 = Digitalisering .AdministrativeUnitReference
				searchCriterion.AdministrativeUnit = AdmUnit; 
				foreach (AcadreServiceV7.CaseSearchResponseType foundCase in caseService.SearchCases(searchCriterion))
				{
					string caseIDtemp = foundCase.CaseFileReference;
					AcadreServiceV7.CaseFileType3 Case = caseService.GetCase(caseIDtemp);
					if (Case.CustomFields.df1.Contains("Emply ID:" + employeeInformation.EmplyID))
					{
						CaseExists = true;
						caseID = caseIDtemp;
						break;
					}
				}

				// Hvis sagen allerede eksisterer, så skal der ikke gøres yderligere
				if(Properties.Settings.Default.IsTest) CaseExists = false; // til test
				
				if (!CaseExists)
				{
					string contactGUID;
					string contactName;
					// look up contact by cprnumber
					var searchContactCriterion = new AcadreServiceV7.SearchContactCriterionType2();
					searchContactCriterion.ContactTypeName = "Person";
					searchContactCriterion.SearchTerm = employeeInformation.cpr;

					var foundContacts = contactService.SearchContacts(searchContactCriterion);
					if (foundContacts.Length > 0)
					{
						// contact already exists, read GUID and name
						contactGUID = foundContacts.First().GUID;
						contactName = foundContacts.First().ContactTitle;
					}
					else
					{
						// forsøger at finde CPR i CPR Broker
						CPRBrokerWrapper.SimplePerson simplePerson;
						try
						{
							simplePerson = CPRBrokerWrapper.GetSimplePerson(Properties.Settings.Default.CPRBroker_ApplicationToken, Properties.Settings.Default.CPRBroker_UserToken, employeeInformation.cpr);
						}
						catch (Exception e)
						{
							throw new Exception("Det indtastede CPR-nummer ("+employeeInformation.cpr+") kunne ikke findes i CPR-registret" + Properties.Settings.Default.Fejl40indikator, e);
						}
						// contact doesn't exist - create it and assign GUID
						var contact = new AcadreServiceV7.PersonType2();
						contact.PersonCivilRegistrationIdentifierStatusCode = "0";
						contact.PersonCivilRegistrationIdentifier = employeeInformation.cpr;
						contact.PersonNameForAddressingName = contactName = simplePerson.FullName;
						contactGUID = contactService.CreateContact(contact);
					}

                    var CategoryList = configurationService.GetCategoryList(new AcadreServiceV7.EmptyRequestType()).ToList().Where(x => x.Principle.Literal == "KL Koder");
                    string KLEtext = CategoryList.Where(x => x.Literal == KLE).First().Title;
                    var createCaseRequest = new AcadreServiceV7.CreateCaseRequestType();

					AcadreServiceV7.CaseFileType3 caseFile = new AcadreServiceV7.CaseFileType3();
					caseFile.CaseFileTypeCode = "PERSAG";
					caseFile.Year = DateTime.Now.Year.ToString();
					caseFile.CreationDate = DateTime.Now;
					caseFile.CaseFileTitleText = string.Format("{0} - {1} - {2}", employeeInformation.cpr,contactName,KLEtext);
					caseFile.TitleAlternativeText = contactName;
					caseFile.RestrictedFromPublicText = "2"; // Aktindsigt, 2 = Delvis
					caseFile.CaseFileStatusCode = "B";
					caseFile.CaseFileDisposalCode = "K5";
					caseFile.DeletionCode = "P1800D";
					caseFile.AccessCode = "PE";

                    caseFile.AdministrativeUnit = new AcadreServiceV7.AdministrativeUnitType[]
					{
								new AcadreServiceV7.AdministrativeUnitType() { AdministrativeUnitReference = employeeInformation.AcadreOrgId.ToString() } // AcadreOrgID fra view
					};

					caseFile.CustomFieldCollection = new AcadreServiceV7.CustomField[]
					{
								new AcadreServiceV7.CustomField(){Name = "df1",Value = "Ansættelsessag - " + employeeInformation.DateOfHire + " - " + employeeInformation.OrgNavn + " - " + "Emply ID:" + employeeInformation.EmplyID} // OrgNavn fra view
								,new AcadreServiceV7.CustomField(){Name = "df25",Value = contactGUID} //contactGUID
					};

					caseFile.Classification = new AcadreServiceV7.ClassificationType
					{
						Category = new AcadreServiceV7.CategoryType[] {
								new AcadreServiceV7.CategoryType(){ Principle="KL Koder", Literal = KLE }
								,new AcadreServiceV7.CategoryType(){ Principle="Facetter", Literal = "G01"}
							}
					};

					caseFile.Party = new AcadreServiceV7.PartyType[] { new AcadreServiceV7.PartyType() {
								CreationDate = DateTime.Now
								,ContactReference = contactGUID
								,PublicAccessLevelReference = "3"
								,IsPrimary = true
							} };

					createCaseRequest.CaseFile = caseFile;

					var userList = configurationService.GetUserList(new AcadreServiceV7.EmptyRequestType()).ToList();
					var user = userList.SingleOrDefault(ut => ut.Id == employeeInformation.AcadreLederId.ToString()); // AcadreLederId fra view
					if (user != null)
					{
						createCaseRequest.CaseFile.CaseFileManagerReference = user.Id;
					}

					var createCaseResponse = caseService.CreateCase(createCaseRequest);
					// check for multicase (samlesag) response.
					if (createCaseResponse.CreateCaseAndAMCResult == AcadreServiceV7.CreateCaseAndAMCResultType.CaseNotCreatedAndListAMCReceived)
					{
						// create the case in all the multicases
						createCaseRequest.MultiCaseIdentifiers = createCaseResponse.MultiCaseIdentifiers;
						createCaseResponse = createCaseResponse = caseService.CreateCase(createCaseRequest);
					}
					caseID = createCaseResponse.CaseFileIdentifier;
					AcadreServiceV7.CaseFileType3 Case = caseService.GetCase(caseID);
					employeeInformation.CaseFileNumberIdentifier = Case.CaseFileNumberIdentifier;
				}
				else
				{
					throw new Exception("Der eksisterer allerede en personalesag på denne ansættelse i Emply. Link til sag: " + Properties.Settings.Default.Acadre_Caselink + caseID + Properties.Settings.Default.Fejl40indikator);
				}
				
				// Tilføjer Personlige oplysninger, Ansøgning, CV og eventuel ekstra dokumenter samt arbejds- og opholdstilladelse

				// Dokument metadata for alle dokumenter:
				var createMainDocumentRequest = new AcadreServiceV7.CreateMainDocumentRequestType2();
				AcadreServiceV7.DocumentType document = new AcadreServiceV7.DocumentType();
				document.DocumentCategoryCode = "Henvend";
				document.DocumentPaperStorageIndicator = false;
				document.DocumentStatusCode = "B";
				createMainDocumentRequest.Document = document;
				var documentLink = new AcadreServiceV7.DocumentLinkType();
				//documentLink.DocumentLinkIdentifier = "0";
				//documentLink.DocumentLinkTypeCode = "H";
				//documentLink.LinkDate = DateTime.Now;
				createMainDocumentRequest.DocumentLink = documentLink;
				var record = new AcadreServiceV7.RecordType2();
				record.AccessCode = "PE";
				//record.CreationYear = "2015";
				record.RecordSerialNumber = "1";
				record.CaseFileReference = caseID;
				record.RegistrationDate = DateTime.Now;
				record.DocumentDateUnknownIndicator = false;
				record.DocumentDate = DateTime.Now;
				record.PublicationIndicator = "2";
				record.RecordPaperStorageIndicator = false;
				record.DocumentTypeCode = "I";
				record.RecordStatusCode = "J";
				createMainDocumentRequest.Record = record;

				// Personlige oplysninger
				document.DocumentTitleText = "Personlige oplysninger fra ansøgning";
				record.DescriptionText = "Oplysninger opgivet af medarbejder ved ansøgning i Emply";
				createMainDocumentRequest.FileName = employeeInformation.PersonalInformation.filename;
				createMainDocumentRequest.XMLBinary = employeeInformation.PersonalInformation.content;
				CreateMainDocument_WithFileTypeErrorHandling(documentService, createMainDocumentRequest);

				// Ansøgning
				document.DocumentTitleText = "Ansøgning";
				record.DescriptionText = "Ansøgning lagt op af medarbejder på Emply";
				createMainDocumentRequest.FileName = employeeInformation.CoverLetter.filename;
				createMainDocumentRequest.XMLBinary = employeeInformation.CoverLetter.content;
				CreateMainDocument_WithFileTypeErrorHandling(documentService, createMainDocumentRequest);

				// CV (ikke obligatorisk)
				if (employeeInformation.CV.content != null)
				{
					document.DocumentTitleText = "CV";
					record.DescriptionText = "CV lagt op af medarbejder på Emply";
					createMainDocumentRequest.FileName = employeeInformation.CV.filename;
					createMainDocumentRequest.XMLBinary = employeeInformation.CV.content;
					CreateMainDocument_WithFileTypeErrorHandling(documentService, createMainDocumentRequest);
				}

				// Ekstradokumenter (ikke obligatorisk)
				int cnt = 0;
				foreach (FileInformation extradocumentsfile in employeeInformation.ExtraDocuments)
				{
					document.DocumentTitleText = extradocumentsfile.filename;
					record.DescriptionText = "Ekstra dokument lagt op af medarbejder på Emply med filnavn: '" + extradocumentsfile.filename + "'. Kunne fx være eksamensbevis, udtalelser osv.";
					createMainDocumentRequest.FileName = extradocumentsfile.filename;
					createMainDocumentRequest.XMLBinary = extradocumentsfile.content;
					CreateMainDocument_WithFileTypeErrorHandling(documentService, createMainDocumentRequest);
					cnt++;
				}

				// Arbejds- og Opholdstilladelse (ikke obligatorisk)
				if (employeeInformation.DocumentationWorkPermit.content != null)
				{
					document.DocumentTitleText = "Arbejds- og Opholdstilladelse";
					record.DescriptionText = "Arbejds- og Opholdstilladelse lagt op af medarbejder på Emply";
					createMainDocumentRequest.FileName = employeeInformation.DocumentationWorkPermit.filename;
					createMainDocumentRequest.XMLBinary = employeeInformation.DocumentationWorkPermit.content;
					CreateMainDocument_WithFileTypeErrorHandling(documentService, createMainDocumentRequest);
				}
			}
			catch (Exception e)
			{
				throw new Exception("Kunne ikke færdiggøre journalisering i Acadre fordi: " + e.Message);
			}
			return int.Parse(caseID);
		}

		public void CreateMainDocument_WithFileTypeErrorHandling(AcadreServiceV7.MainDocumentService7 mainDocumentService,AcadreServiceV7.CreateMainDocumentRequestType2 createDocumentRequest)
		{
			try
			{
				mainDocumentService.CreateMainDocument(createDocumentRequest);
			} catch (Exception e)
			{
				if (e.Message.Contains("library does not support") || e.Message.Contains("The INSERT statement conflicted with the FOREIGN KEY constraint"))
				{
					// Løn og Personale ønsker ikke at ugyldige filer gemmes før de kan åbnes i Acadre (eksempelvis lyd- og videofiler). Derefter er nedenstående udkommenteret
					// createDocumentRequest.FileName = createDocumentRequest.FileName +  ".renamed.txt";
					// mainDocumentService.CreateMainDocument(createDocumentRequest);
				}
				else
				{
					throw e;
				}
			}
		}
	}
}