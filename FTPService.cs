﻿using System.IO;
using System.Collections.Generic;
using WinSCP;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System;

namespace EmplySFTPToAcadre
{
	class FTPService
	{
		SessionOptions sessionOptions;
		string workdir = Properties.Settings.Default.Workdir; 

		public FTPService()
		{			
			sessionOptions = new SessionOptions
			{
				Protocol = Protocol.Sftp,
				HostName = Properties.Settings.Default.SFTP_HostName,
				UserName = Properties.Settings.Default.SFTP_UserName,
				Password = Properties.Settings.Default.SFTP_Password,
				SshHostKeyFingerprint = Properties.Settings.Default.SFTP_SshHostKeyFingerprint,
			};
		}

		internal List<string> getDirectories()
		{
			using (Session session = new Session())
			{
				List<string> directories = new List<string>();
				// Connect
				session.Open(sessionOptions);
				RemoteDirectoryInfo rdi = session.ListDirectory("/.");
				foreach(RemoteFileInfo remoteFileInfo in rdi.Files)
				{
					if(remoteFileInfo.IsDirectory && remoteFileInfo.Name != ".." && remoteFileInfo.Name != ".")
					{
						directories.Add(remoteFileInfo.Name);
					}
				}
				return directories;
			}
			
		}

		internal void deleteDirectory(string directory)
		{
			using (Session session = new Session())
			{
				// Connect
				session.Open(sessionOptions);
				
				session.RemoveFiles("/" + directory);
			}
		}

		internal EmployeeInformation GetEmployeeInformation(string directory)
		{
			EmployeeInformation employeeInformation = new EmployeeInformation();
			
			using (Session session = new Session())
			{
				// Indlæser XML file
				TransferOptions transferOptions = new TransferOptions
				{
					TransferMode = TransferMode.Automatic,
					FileMask = "*.xml",
				};

				XElement xmlTree = XElement.Load(new StreamReader(new MemoryStream(GetFileContent("/" + directory, session,transferOptions)), Encoding.GetEncoding("ISO-8859-1")));

				// Opbygning af XML-fil er angivet i Emply og kan tilgås gennem følgende link (forudsat at man har adgang): https://syddjurs.emply.net/recruitment/ExportXmlDetails.aspx?exportDocumentId=36ac7265-bdbd-49b3-bdd5-351e2bda5661&languageKey=da-DK
				// Indlæser data som er nødvendig for at oprette personalesag
				employeeInformation.cpr = xmlTree.XPathSelectElement("./cpr").Value.Replace("-", "").Trim(); // CPR nummer på medarbejder opgivet af leder (OBS! fritekst)
				employeeInformation.LOSOrgId = int.Parse(xmlTree.XPathSelectElement("./DepartmentID").Value); // LOSorgID til ansættelsessted vacancy.Department.identifier
				employeeInformation.EmplyID = xmlTree.XPathSelectElement("./id").Value; // Unikt medarbejder ID fra Emply. Bruges til at identificere om sag allerede eksisterer
				employeeInformation.DateOfHire = xmlTree.XPathSelectElement("./DateOfHire").Value; // Til Sagsindhold (dato kan afvige fra faktisk tiltrædelsesdato, hvis det forhandles igennem med medarbejder efterfølgende)
				employeeInformation.projectmanagerEmail = xmlTree.XPathSelectElement("./projectmanagerEmail").Value; // Anvendes til at skrive til Projektleder hvis der sker brugerfejl (fejl 40)
				employeeInformation.projectmanagerName = xmlTree.XPathSelectElement("./projectmanagerName").Value; // Anvendes til at skrive til Projektleder hvis der sker brugerfejl (fejl 40)
				employeeInformation.specialFormIndicator = xmlTree.XPathSelectElement("./specialFormIndicator").Value; // Anvendes til at bestemme om en ansøgning er standard eller ej ("" = standard). I virkligheden er det svaret på om de har taget førstehjælpskursus. "Tilfældigvis" er det et obligatorisk spørgsmål som kun eksisterer for ikke standard ansøgninger.
				employeeInformation.vacancyID = xmlTree.XPathSelectElement("./vacancyID").Value; // Anvendes til at kunne sende et link direkte ind på projektet i Emply 

				// Indlæser data som skal gemmes på personalesag
				employeeInformation.email = xmlTree.XPathSelectElement("./email").Value;
				employeeInformation.firstname = xmlTree.XPathSelectElement("./firstname").Value;
				employeeInformation.lastname = xmlTree.XPathSelectElement("./lastname").Value;
				employeeInformation.birthday = xmlTree.XPathSelectElement("./birthday").Value;
				employeeInformation.sex = xmlTree.XPathSelectElement("./sex").Value;
				employeeInformation.address = xmlTree.XPathSelectElement("./address").Value;
				employeeInformation.zipcode = xmlTree.XPathSelectElement("./zipcode").Value;
				employeeInformation.city = xmlTree.XPathSelectElement("./city").Value;
				employeeInformation.phonenumber = xmlTree.XPathSelectElement("./phonenumber").Value;
				employeeInformation.educationlevel = xmlTree.XPathSelectElement("./educationlevel").Value;
				employeeInformation.educationfield = xmlTree.XPathSelectElement("./educationfield").Value;
				employeeInformation.educationtitle = xmlTree.XPathSelectElement("./educationtitle").Value;
				employeeInformation.latestEmployment = xmlTree.XPathSelectElement("./latestEmployment").Value;
				employeeInformation.latestEmployer = xmlTree.XPathSelectElement("./latestEmployer").Value;
				employeeInformation.yearsExperience = xmlTree.XPathSelectElement("./yearsExperience").Value;
				employeeInformation.handicapRight = xmlTree.XPathSelectElement("./handicapRight").Value;
				employeeInformation.handicapDescription = xmlTree.XPathSelectElement("./handicapDescription").Value;

				// Indlæser ansøgning
				employeeInformation.CoverLetter.filename = xmlTree.XPathSelectElement("./coverletter/file").Attribute("name").Value;
				employeeInformation.CoverLetter.content = GetFileContent("/" + directory + "/" + xmlTree.XPathSelectElement("./coverletter/file").Value,session);
				
				// Indlæser CV
				string CVfilepath = xmlTree.XPathSelectElement("./cv/file").Value;
				if (CVfilepath != "")
				{
					employeeInformation.CV.filename = xmlTree.XPathSelectElement("./cv/file").Attribute("name").Value;
					employeeInformation.CV.content = GetFileContent("/" + directory + "/" + CVfilepath,session);
				}
					
				// Indlæser ekstra dokumenter
				var ed = xmlTree.XPathSelectElements("./extradocuments/file");
				foreach (var e in ed)
				{
					FileInformation fileInformation = new FileInformation();
					fileInformation.filename = e.Attribute("name").Value;
					fileInformation.content = GetFileContent("/" + directory + "/" + e.Value, session);
					employeeInformation.ExtraDocuments.Add(fileInformation);
				}


				employeeInformation.IsDanishCitizen = xmlTree.XPathSelectElement("./statsborgerskab/ErDanskStatsborger").Value;
				employeeInformation.HasWorkPermit = xmlTree.XPathSelectElement("./statsborgerskab/HarArbejdsOgOpholdstilladelse").Value;
				string documentationWorkPermitfilepath = xmlTree.XPathSelectElement("./statsborgerskab/Dokumentation").Value;
				if (documentationWorkPermitfilepath != "")
				{
					employeeInformation.DocumentationWorkPermit.filename = "Arbejds- og Opholdstilladelse" + Path.GetExtension(documentationWorkPermitfilepath);
					employeeInformation.DocumentationWorkPermit.content = GetFileContent("/" + directory + "/" + documentationWorkPermitfilepath, session);
				}
			}
			return employeeInformation;
		}

		internal void MoveToLocal(string remoteDirectory, string EmplyID)
		{
			using (Session session = new Session())
			{
				// Connect
				string employeeID;
				session.Open(sessionOptions);
				if (EmplyID != null) employeeID = EmplyID; else employeeID = remoteDirectory;
				session.GetFiles("\\" + remoteDirectory, workdir + "\\failed\\" + employeeID);

				session.RemoveFiles("/" + remoteDirectory);
			}
		}

		private byte[] GetFileContent(string remoteFilepath, Session session)
		{
			byte[] contents = null;

			if (!session.Opened)
			session.Open(sessionOptions);
			string localTempFilepath = Path.GetTempFileName();
			int attempts = 0;
			while(true)// for (int attempts = 0; attempts < 2; attempts++)
			{
				try
				{
					// Download to a temporary file
					session.GetFiles(remoteFilepath, localTempFilepath).Check();

					// Read the file contents
					contents = File.ReadAllBytes(localTempFilepath);
					File.Delete(localTempFilepath);
					break;
				}
				catch (Exception e)
				{
					if (attempts >= 2)
					{
						File.Delete(localTempFilepath);
						throw e;
					}
					System.Threading.Thread.Sleep(2000);
				}
				attempts++;
			}
			return contents;
		}

		private byte[] GetFileContent(string remotePath, Session session, TransferOptions transferOptions)
		{
			byte[] contents;
			string localTempFilepath = "";
			if (!session.Opened)
				session.Open(sessionOptions);
			string localTempPath = Path.GetTempPath();
			int attempts = 0;
			while (true)
			{ 
				try
				{
					// Download to a temporary folder
					TransferOperationResult transferOperationResult = session.GetFiles(remotePath, localTempPath, false, transferOptions);
					localTempFilepath = transferOperationResult.Transfers[0].Destination;
					transferOperationResult.Check();
					// Read the file contents
					contents = File.ReadAllBytes(localTempFilepath);
					File.Delete(localTempFilepath);
					break;
				}
				catch (Exception e)
				{
					if (attempts >= 2)
					{
						if (localTempFilepath != "")
							File.Delete(localTempFilepath);
						throw e;
					}
					System.Threading.Thread.Sleep(2000);
				}
				attempts++;
			}
			return contents;
		}
	}
}