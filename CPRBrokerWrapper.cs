﻿using System;
using System.Text.RegularExpressions;

namespace EmplySFTPToAcadre
{
	public static class CPRBrokerWrapper
	{
		public static SimplePerson GetSimplePerson(string applicationToken, string userToken, string cpr)
		{
			SimplePerson person = new SimplePerson();
			CPRBroker.PartSoap12 partClient = new CPRBroker.PartSoap12Client();
			CPRBroker.ApplicationHeader applicationHeader = new CPRBroker.ApplicationHeader
			{
				ApplicationToken = applicationToken,
				UserToken = userToken
			};
			CPRBroker.GetUuidOutputType uuid = partClient.GetUuid(new CPRBroker.GetUuidRequest(applicationHeader, cpr)).GetUuidOutput;
			if (uuid.StandardRetur.StatusKode != "200")
			{
				throw new CPRBrokerWrapperExpeption(String.Format("CPRBrokerWrapper: Call to GetUuid failed. StatusKode:{0}. FejlbeskedTekst: {1}", uuid.StandardRetur.StatusKode, uuid.StandardRetur.FejlbeskedTekst));
			}
			else
			{
				// Read: Finds and returns a single person object. It will return the latest registration within the specified range. 
				// It first looks in the local database, and attempts external data providers if no data is found locally.
				CPRBroker.LaesOutputType output = partClient.Read(new CPRBroker.ReadRequest(applicationHeader, new CPRBroker.SourceUsageOrderHeader(), new CPRBroker.LaesInputType { UUID = uuid.UUID })).LaesOutput;
				if (output.StandardRetur.StatusKode != "200")
				{
					throw new CPRBrokerWrapperExpeption(String.Format("CPRBrokerWrapper: Call to GetUuid failed. StatusKode:{0}. FejlbeskedTekst: {1}", output.StandardRetur.StatusKode, output.StandardRetur.FejlbeskedTekst));
				}
				else
				{
					CPRBroker.RegistreringType1 laesResultatItem;
					if (output.LaesResultat.Item is CPRBroker.RegistreringType1)
					{
						laesResultatItem = (CPRBroker.RegistreringType1)output.LaesResultat.Item;
					}
					else
					{
						throw new CPRBrokerWrapperExpeption(String.Format("CPRBrokerWrapper: Unexpected RegistreringType derived type: {0}", output.LaesResultat.Item.GetType().Name));
					}
					person.FirstName = laesResultatItem.AttributListe.Egenskab[0].NavnStruktur.PersonNameStructure.PersonGivenName;
					person.MiddleName = laesResultatItem.AttributListe.Egenskab[0].NavnStruktur.PersonNameStructure.PersonMiddleName;
					person.Surname = laesResultatItem.AttributListe.Egenskab[0].NavnStruktur.PersonNameStructure.PersonSurnameName;

					CPRBroker.CprBorgerType registerOplysningItem = (CPRBroker.CprBorgerType)laesResultatItem.AttributListe.RegisterOplysning[0].Item;

					person.NameAddressProtection = registerOplysningItem.NavneAdresseBeskyttelseIndikator;

					if (registerOplysningItem.FolkeregisterAdresse != null)
					{
						CPRBroker.AdresseBaseType adresse = registerOplysningItem.FolkeregisterAdresse.Item;
						if (adresse is CPRBroker.DanskAdresseType)
						{
							CPRBroker.AddressPostalType danishAddress = ((CPRBroker.DanskAdresseType)adresse).AddressComplete.AddressPostal;
							string streetNameAndNumber = danishAddress.StreetName + " " + ParseAddressField(danishAddress.StreetBuildingIdentifier);
							string floorSuite = ParseAddressField(danishAddress.FloorIdentifier) + "," + ParseAddressField(danishAddress.SuiteIdentifier);
							floorSuite = Regex.Replace(floorSuite, @"(^|\s)(st|tv|th)\b", "$1$2.", RegexOptions.IgnoreCase);
							person.AddressLine1 = (streetNameAndNumber + (floorSuite == "" ? "" : ", " + floorSuite)).Trim();
							person.AddressLine2 = (ParseAddressField(danishAddress.PostCodeIdentifier) + " " + ParseAddressField(danishAddress.DistrictName)).Trim();
						}
						else if (adresse is CPRBroker.GroenlandAdresseType)
						{
							CPRBroker.AddressCompleteGreenlandType greenlandAddress = ((CPRBroker.GroenlandAdresseType)adresse).AddressCompleteGreenland;
							string streetNameAndNumber = greenlandAddress.StreetName + " " + ParseAddressField(greenlandAddress.StreetBuildingIdentifier);
							string floorSuite = ParseAddressField(greenlandAddress.FloorIdentifier) + "," + ParseAddressField(greenlandAddress.SuiteIdentifier);
							person.AddressLine1 = (streetNameAndNumber + (floorSuite == "" ? "" : ", " + floorSuite)).Trim();
							person.AddressLine2 = (ParseAddressField(greenlandAddress.PostCodeIdentifier) + " " + ParseAddressField(greenlandAddress.DistrictName)).Trim();
							person.AddressLine3 = "Grønland";
						}
						else if (adresse is CPRBroker.VerdenAdresseType)
						{
							CPRBroker.ForeignAddressStructureType foreignAddress = ((CPRBroker.VerdenAdresseType)adresse).ForeignAddressStructure;
							person.AddressLine1 = foreignAddress.PostalAddressFirstLineText;
							person.AddressLine2 = foreignAddress.PostalAddressSecondLineText;
							person.AddressLine3 = foreignAddress.PostalAddressThirdLineText;
							person.AddressLine4 = foreignAddress.PostalAddressFourthLineText;
							person.AddressLine5 = foreignAddress.PostalAddressFifthLineText;
						}
						else
						{
							throw new CPRBrokerWrapperExpeption(String.Format("CPRBrokerWrapper: Unexpected AdresseBaseType derived type: {0}", adresse.GetType().Name));
						}
					}
				}
			}
			return person;
		}

		private static string ParseAddressField(string field)
		{
			// return empty string if null. Remove leading zeros
			return field == null ? "" : field.TrimStart('0');
		}

		public class SimplePerson
		{
			private bool aNameAddressProtection;
			private string aFirstName;
			private string aMiddleName;
			private string aSurname;
			private string aAddressLine1;
			private string aAddressLine2;
			private string aAddressLine3;
			private string aAddressLine4;
			private string aAddressLine5;

			public bool NameAddressProtection
			{
				get { return aNameAddressProtection; }
				set { aNameAddressProtection = value; }
			}

			public string FirstName
			{
				get { return aFirstName ?? ""; }
				set { aFirstName = value; }
			}

			public string MiddleName
			{
				get { return aMiddleName ?? ""; }
				set { aMiddleName = value; }
			}

			public string Surname
			{
				get { return aSurname ?? ""; }
				set { aSurname = value; }
			}

			public string AddressLine1
			{
				get { return aAddressLine1 ?? ""; }
				set { aAddressLine1 = value; }
			}

			public string AddressLine2
			{
				get { return aAddressLine2 ?? ""; }
				set { aAddressLine2 = value; }
			}

			public string AddressLine3
			{
				get { return aAddressLine3 ?? ""; }
				set { aAddressLine3 = value; }
			}

			public string AddressLine4
			{
				get { return aAddressLine4 ?? ""; }
				set { aAddressLine4 = value; }
			}

			public string AddressLine5
			{
				get { return aAddressLine5 ?? ""; }
				set { aAddressLine5 = value; }
			}

			public string FullName
			{
				get
				{ return (FirstName + " " + MiddleName + " " + Surname).Replace("  ", " "); }
			}

			public string EnvelopeAddress
			{
				get
				{
					string result = this.FullName;
					result += this.AddressLine1 == "" ? "" : "\n" + this.aAddressLine1;
					result += this.AddressLine2 == "" ? "" : "\n" + this.aAddressLine2;
					result += this.AddressLine3 == "" ? "" : "\n" + this.aAddressLine3;
					result += this.AddressLine4 == "" ? "" : "\n" + this.aAddressLine4;
					result += this.AddressLine5 == "" ? "" : "\n" + this.aAddressLine5;
					return result;
				}
			}
		}

		// returns true if string cpr is a 10 number long int with the first 4 digits being a valid day and month
		public static bool IsValidCPR(string cpr)
		{
			if (cpr.Length != 10) return false;
			uint theNum = 0;
			if (!uint.TryParse(cpr, out theNum)) return false;
			if (int.Parse(cpr.Substring(0, 2)) > 31 || int.Parse(cpr.Substring(0, 2)) == 0) return false;
			if (int.Parse(cpr.Substring(2, 2)) > 12 || int.Parse(cpr.Substring(2, 2)) == 0) return false;
			return true;
		}

		public class CPRBrokerWrapperExpeption : Exception
		{
			public CPRBrokerWrapperExpeption(string message) : base(message) { }
		}


	}
}

