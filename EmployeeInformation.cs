﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EmplySFTPToAcadre
{
	public class EmployeeInformation
	{
		public EmployeeInformation()
		{
			CV = new FileInformation();
			CoverLetter = new FileInformation();
			DocumentationWorkPermit = new FileInformation();
			PersonalInformation = new FileInformation();
			ExtraDocuments = new List<FileInformation>();
		}
		public string cpr { get; set; }
		public string EmplyID { get; set; }
		public string DateOfHire { get; set; }
		public string address { get; set; }
		public string firstname { get; set; }
		public string lastname { get; set; }
		public string birthday { get; set; }
		public string sex { get; set; }
		public string email { get; set; }
		public FileInformation CV { get; set; }
		public FileInformation CoverLetter { get; set; }
		public FileInformation DocumentationWorkPermit { get; set; }
		public FileInformation PersonalInformation { get; set; }
		public List<FileInformation> ExtraDocuments { get; set; }
		public int LOSOrgId { get; set; }
		public int AcadreLederId { get; set; }
		public int AcadreOrgId { get; set; }
		public string OrgNavn { get; set; }
		public int AcadreCaseId { get; set; }
		public string zipcode { get; set; }
		public string city { get; set; }
		public string phonenumber { get; set; }
		public string educationlevel { get; set; }
		public string educationfield { get; set; }
		public string educationtitle { get; set; }
		public string latestEmployment { get; set; }
		public string latestEmployer { get; set; }
		public string yearsExperience { get; set; }
		public string handicapRight { get; set; }
		public string handicapDescription { get; set; }
		public string IsDanishCitizen { get; set; }
		public string HasWorkPermit { get; set; }
		public string LederEmail { get; set; }
		public string projectmanagerEmail { get; set; }
		public string specialFormIndicator { get; set; }
		public string vacancyID { get; set; }
		public string CaseFileNumberIdentifier { get; set; }
		public string projectmanagerName { get; set; }

		public void CreateTextFile()
		{
			string nl = "\r\n";
			string Documentation = "";
			if(DocumentationWorkPermit.content != null)
			{
				Documentation = "Filnavn: " + DocumentationWorkPermit.filename;
			}
			else
			{
				Documentation = "Ingen vedhæftning";
			}
			string txtfile =
				"Personlige oplysninger" + nl +
				"E-mail: " + email + nl +
				"Fornavn: " + firstname + nl +
				"Efternavn: " + lastname + nl +
				"Fødselsdato: " + birthday + nl +
				"Køn: " + sex + nl +
				"Telefon: (mobil) " + phonenumber + nl +
				"Adresse: " + address + nl +
				"Postnr.: " + zipcode + nl +
				"By: " + city + nl + nl +
				"Ansøgning og CV" + nl +
				"Uddannelsesniveau: " + educationlevel + nl +
				"Fagområde for uddannelse: " + educationfield + nl +
				"Titel på uddannelse: " + educationtitle + nl +
				"Seneste stilling: " + latestEmployment + nl +
				"Seneste arbejdsgiver: " + latestEmployer + nl +
				"Antal års erhvervserfaring: " + yearsExperience + nl +
				"Anmoder du om fortrinsret for handicappede?: " + handicapRight + nl +
				"Hvis ja, beskriv da kort din funktionsnedsættelse: " + handicapDescription + nl +
				"Er du dansk statsborger?: " + IsDanishCitizen + nl +
				"Hvis nej, har du arbejds - og opholdstilladelse?: " + HasWorkPermit + nl +
				"Hvis ja, vedhæft dokumentation: " + Documentation + nl +
				"";
			PersonalInformation.content = System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(txtfile);
			PersonalInformation.filename = "data.txt";
		}
	}
}
