﻿using System;
using System.Net;
using System.Net.Mail;

namespace EmplySFTPToAcadre
{
	public class MailService
	{
		private SmtpClient smtpClient; 
		private MailMessage mailMessage;
		private readonly string nl = "\r\n";

		public MailService()
		{
			smtpClient = new SmtpClient(Properties.Settings.Default.MailService_Server)
			{
				Credentials = new NetworkCredential {
					UserName = Properties.Settings.Default.MailService_UserName,
					Password = Properties.Settings.Default.MailService_Password,
					Domain = Properties.Settings.Default.MailService_Domain }
			};
		}

		public void SendReceiptToEmployeeLeader(EmployeeInformation employeeInformation)
		{
			string Subject = "Kvittering: Autojournalisering af medarbejderoplysninger fra Emply";
			string Body = "Kære " + employeeInformation.projectmanagerName + nl + nl +
						  "Du modtager denne email som ansvarlig for et ansættelsesforløb i rekrutteringssystemet Emply." + nl + nl +
						  "De informationer, som kandidaten har opgivet, er blevet autojournaliseret i Acadre sag " + employeeInformation.CaseFileNumberIdentifier + "." + nl + nl +
						  "Følgende dokumenter er blevet lagt ind i Acadre:" + nl +
						  "\t-  Personlige Informationer" + nl +
						  "\t-  Vedhæftede filer" + nl +
						  "\t          \u2022  Ansøgning" + nl +
						  (employeeInformation.CV.filename != null ? "\t          \u2022  CV" + nl : "") +
						  (employeeInformation.ExtraDocuments.Count != 0 ? "\t          \u2022  " + employeeInformation.ExtraDocuments.Count.ToString() + " Ekstra dokument(er)" + nl : "") +
						  (employeeInformation.DocumentationWorkPermit.filename != null ? "\t          \u2022  Dokumentation af Arbejds- og opholdstilladelse" + nl : "") + nl + nl +
						  "Husk at journalisere stillingsopslag og eventuelt straffeattest/børneattest, tavshedserklæring samt dokumentation for tidligere ansættelse, omsorgsdage og seniordage, hvis det er aktuelt." + nl + nl +
						  "Link til projektet i Emply: " + Properties.Settings.Default.Emply_VacancyURL + employeeInformation.vacancyID + nl +
						  "Link til sag i Acadre via browser: " + Properties.Settings.Default.Acadre_Caselink + employeeInformation.AcadreCaseId.ToString() + nl + nl +
						  "Denne mail er system genereret og kan ikke besvares. Skriv til " + Properties.Settings.Default.Admin_email + " hvis du har spørgsmål eller oplever fejl.";
			if (Properties.Settings.Default.IsTest)
			{
				mailMessage = new MailMessage(Properties.Settings.Default.MailService_EmailAdress, Properties.Settings.Default.Admin_email, Subject, Body);
				//mailMessage.IsBodyHtml = true;
			}
			else
			{
				mailMessage = new MailMessage(Properties.Settings.Default.MailService_EmailAdress, employeeInformation.projectmanagerEmail);
				mailMessage.Subject = Subject;
				mailMessage.Body = Body;
				//mailMessage.IsBodyHtml = true;
			}
			
			smtpClient.Send(mailMessage);
		}

		public void SendErrorToProjectManager(Exception e, EmployeeInformation employeeInformation)
		{
			string Subject = "Autojournalisering mislykkedes";
			string Body = "Kære " + employeeInformation.projectmanagerName + nl + nl +
						  "Du modtager denne email som ansvarlig for et ansættelsesforløb i rekrutteringssystemet Emply." + nl + nl +
						  "Autojournalisering af kandidat i Emply med ID " + employeeInformation.EmplyID + " mislykkedes. Årsag:"+nl
						  + e.Message.Replace(Properties.Settings.Default.Fejl40indikator, "") +nl+nl+ "Du kan forsøge igen på dette link til Emply projektet: " + Properties.Settings.Default.Emply_VacancyURL + employeeInformation.vacancyID + nl + nl +
						  "Denne mail er system genereret og kan ikke besvares. Skriv til " + Properties.Settings.Default.Admin_email + " hvis du har spørgsmål eller oplever fejl.";
			if (Properties.Settings.Default.IsTest)
				mailMessage = new MailMessage(Properties.Settings.Default.MailService_EmailAdress, Properties.Settings.Default.Admin_email, Subject, Body);
			else
			{
				mailMessage = new MailMessage(Properties.Settings.Default.MailService_EmailAdress, employeeInformation.projectmanagerEmail);
				mailMessage.Subject = Subject;
				mailMessage.Body = Body;
			}
			
			smtpClient.Send(mailMessage);
		}

		public void SendErrorToAdmin(Exception e, EmployeeInformation employeeInformation)
		{
			
			mailMessage = new MailMessage(Properties.Settings.Default.MailService_EmailAdress, Properties.Settings.Default.Admin_email, "Error - EmplySFTPToAcadre", "Windows service EmplySFTPToAcadre failed for this Emply candidate: " + employeeInformation.EmplyID +  " with this error message: " + e.Message + "\r\n\r\nThe directory can be found in local folder on server: " + Properties.Settings.Default.Workdir + "/" + employeeInformation.EmplyID);
			smtpClient.Send(mailMessage);
		}

		public void SendErrorToAdmin(Exception e, string failedDirectory)
		{

			mailMessage = new MailMessage(Properties.Settings.Default.MailService_EmailAdress, 
										  "dbvi@syddjurs.dk", 
										  "Error - EmplySFTPToAcadre", 
										  "Windows service EmplySFTPToAcadre failed trying to load files from this directory on the SFTP server: " + failedDirectory + 
										  " with this error message: " + e.Message + "\r\n\r\nThe directory can be found in local folder on server: " + Properties.Settings.Default.Workdir);
			smtpClient.Send(mailMessage);
		}

		public void SendErrorToAdmin(Exception e)
		{
			
			mailMessage = new MailMessage("no-reply@syddjurs.dk", "dbvi@syddjurs.dk", "Error - EmplySFTPToAcadre", "Windows service EmplySFTPToAcadre failed before accessing any data with this error message: \r\n" + e.Message);
			smtpClient.Send(mailMessage);
		}
	}
}