﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmplySFTPToAcadre
{
	class SyncJob : IJob
	{
		public Task Execute(IJobExecutionContext context)
		{
			Console.WriteLine(DateTime.Now.ToLongTimeString() + ": Starting Lookup");
			try
			{
				var ftpService = new FTPService();
				var acadreService = new AcadreService();
				var mailService = new MailService();
				EmployeeInformation employeeInformation;
				List<string> directories = ftpService.getDirectories();
				if (directories.Count > 0) System.Threading.Thread.Sleep(8000); // Make sure that all files are downloaded before starting
				foreach (var directory in directories)
				{
					employeeInformation = null;
				    try
				    {
					// get information on Employee from Emply's SFTP server
					employeeInformation = ftpService.GetEmployeeInformation(directory);

					// Der skal ikke ske autojournalisering af ansøgninger relateret til SOSU elever, SOSU assistent elever og PAU elever
					if (employeeInformation.specialFormIndicator != "")
					{
						ftpService.deleteDirectory(directory);
						continue;
					}
							

					// enrich employee with Acadre values from SOFD
					using (var mdm = new DataModel.MDMEntities())
					{
						var Acadremap = mdm.AcadreMap.SingleOrDefault(a => a.LOSOrgId == employeeInformation.LOSOrgId);
						if (Acadremap != null)
						{
							employeeInformation.AcadreLederId = Acadremap.AcadreLederId;
							employeeInformation.AcadreOrgId = Acadremap.AcadreOrgId;
							employeeInformation.OrgNavn = Acadremap.OrgNavn;
							employeeInformation.LederEmail = Acadremap.LederEmail;
						}
						else
						{
							throw new Exception("Kunne ikke finde den tilknyttede afdeling i Acadre. Afdelingens LOSOrgId: " + employeeInformation.LOSOrgId);
						}
					}
					// Create Text file for Acadre with information about employee
					employeeInformation.CreateTextFile();

					// Create Case in Acadre and fill it with all data and documents from Emply
					employeeInformation.AcadreCaseId = acadreService.CreateCase(employeeInformation);

					// Send an email receipt to the employees manager
					mailService.SendReceiptToEmployeeLeader(employeeInformation);

					// Logger en vellykket sagsoprettelse
					Trace.TraceInformation(DateTime.Now.ToString("G", DateTimeFormatInfo.InvariantInfo) + ": Successfully created Case " + employeeInformation.CaseFileNumberIdentifier + " and sent email to " + employeeInformation.projectmanagerEmail);

					// The employee data from Emply have been archived so now the directory on the Emply SFTP server that contains the employee data can be deleted 
					ftpService.deleteDirectory(directory);
				    }
				    catch (Exception e)
				    {
					Trace.TraceError(DateTime.Now.ToString("G",DateTimeFormatInfo.InvariantInfo) + ": " + e.Message);
					// Move the employee data to local folder for later inspection.
					ftpService.MoveToLocal(directory, employeeInformation != null ? employeeInformation.EmplyID : directory);
					
						if (employeeInformation != null)
						{
							if (e.Message.Contains(Properties.Settings.Default.Fejl40indikator))
							{
								mailService.SendErrorToProjectManager(e, employeeInformation);
							}
							else
								mailService.SendErrorToAdmin(e, employeeInformation);
						}
						else
						mailService.SendErrorToAdmin(e,directory);	
					continue;
				}
			}
				Console.WriteLine(DateTime.Now.ToLongTimeString() + ": Lookup done");
				return Task.CompletedTask;
			}
			catch (Exception e)
			{
				Trace.TraceError(DateTime.Now.ToShortTimeString() + ": " + e.Message);
				return Task.FromException(e);
			}
		}
	}
}
