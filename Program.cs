﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using Topshelf.Quartz;

namespace EmplySFTPToAcadre
{
	class Program
	{
		static void Main(string[] args)
		{
			var rc = HostFactory.Run(x =>                                   //1
			{
				x.Service<WindowsService>(s =>                                   //2
				{
					
					s.WhenStarted(tc => tc.Start());                         //4
					s.WhenStopped(tc => tc.Stop());                          //5
					s.ConstructUsing(name => new WindowsService());                //3

				});
				x.RunAsLocalSystem();                                       //6

				x.SetDescription("Service for transferring data from Emply's SFTP server to Acadre.");                   //7
				x.SetDisplayName("Emply SFTP server data to Acadre");                                  //8
				x.SetServiceName("EmplySFTPtoAcadre");                                  //9
			});                                                             //10

			var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());  //11
			Environment.ExitCode = exitCode;
		}
	}
}
