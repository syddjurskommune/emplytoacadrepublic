﻿namespace EmplySFTPToAcadre
{
	public class FileInformation
	{
		public string filename { get; set; }
		public byte[] content { get; set; }
	}
}